const fs = require('fs')
const html5Lint = require('html5-lint')

for (const filename of process.argv.slice(2)) {
	fs.readFile(filename, 'utf8', function (err, html) {
		if (err) {
			throw err
		}

		html5Lint(html, (err, results) => {
			if (err) {
				process.exit(0)
			}

			results.messages.forEach(function (msg) {
				var type = msg.type
				var message = msg.message

				console.log('HTML5 Lint [%s]: %s', type, message)
				process.exit(1)
			})
		})
	})
}
