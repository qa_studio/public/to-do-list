class TodoItem {
	constructor(parent, title) {
		this.parent = parent
		this.title = title

		this.render()
	}

	render() {
		const item = this.createToDoListElement()
		this.parent.append(item)
	}

	createToDoListElement() {
		const todoItemElement = document.createElement('div')
		todoItemElement.classList.add('todoItem')

		const title = document.createElement('h2')
		title.innerText = this.title

		todoItemElement.append(title)

		return todoItemElement
	}
}

document.addEventListener('DOMContentLoaded', () => {
	const itemlistNew = document.getElementById('itemlist-new')

	const addTodoInput = document.getElementById('add-todo-input')
	const addTodoButton = document.getElementById('add-todo-button')

	addTodoButton.addEventListener('click', () => {
		if (addTodoInput.value.trim() != '') {
			new TodoItem(itemlistNew, addTodoInput.value)
			addTodoInput.value = ''
		}
	})

	new TodoItem(itemlistNew, 'Изучить CI/CD')
})
